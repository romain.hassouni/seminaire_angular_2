export interface User {
  id: number;
  name: string;
  prenom: string;
  email: string;
  password: string;
}

// model d un utilisateur