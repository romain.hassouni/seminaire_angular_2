import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormsModule } from '@angular/forms';
import { User } from '../user';
import { CommonModule } from '@angular/common';
import { NgForm } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { catchError, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-inscription',
  standalone: true,
  imports: [CommonModule,FormsModule,HttpClientModule],
  templateUrl: './inscription.component.html',
  styleUrl: './inscription.component.scss'
})
export class InscriptionComponent {

  constructor(private userService: UserService,private router:Router) { }

  onSubmitForm(form: NgForm): void {
    if (form.valid) {
      this.userService.addUser(form.value);
      alert('Utilisateur ajouté avec succès !'); // Alerte pour indiquer que l'utilisateur a été ajouté avec succès
    } else {
      alert('Le formulaire n\'est pas valide'); // Alerte pour indiquer que le formulaire n'est pas valide
      console.error('Le formulaire n\'est pas valide');
    }
    this.router.navigateByUrl('/seminaires');
  }
 
}