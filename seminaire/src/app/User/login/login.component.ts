import { Component,OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { FormGroup } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { ReactiveFormsModule,Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-login',
  standalone: true,
  imports: [FormsModule,ReactiveFormsModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent implements OnInit {
  loginError: string | null = null;
  constructor(private userService: UserService,private router:Router) { }

  ngOnInit(): void {
  }
  onSubmitForm(form: NgForm): void {
    if (form.valid) {
      const { email, password } = form.value;
      this.userService.login(email, password)
        .then((success) => {
          if (success) {
            alert('Utilisateur connecté avec succès.');
            // Redirection de l'utilisateur ou autres actions en cas de connexion réussie
          } else {alert('Utilisateur introuvable ');
          }
        })
    } else {
      console.error('Le formulaire n\'est pas valide');
    }
    this.router.navigateByUrl('/seminaires');
  }
}