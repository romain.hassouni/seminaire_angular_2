import { Injectable } from '@angular/core';
import { Seminaire } from '../models/site_models';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Timestamp } from 'firebase/firestore';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SeminaireService {
  private seminaires: Seminaire[] = [];
  private seminairesSubject: BehaviorSubject<Seminaire[]> = new BehaviorSubject<Seminaire[]>([]);
  public seminaires$: Observable<Seminaire[]> = this.seminairesSubject.asObservable();

  constructor(private firestore: AngularFirestore) {
    this.getAllSeminaire().subscribe(seminaires => {
      this.seminaires = seminaires;
      this.seminairesSubject.next(seminaires);
    });
  }

  getAllSeminaire(): Observable<Seminaire[]> {
    return this.firestore.collection<Seminaire>('seminaires').valueChanges();
  }

  addSeminaire(formValue: { titre: string, date: Timestamp, intervenant: string, lieu: string, resume: string, domaine: string }): void {
    const seminaire: Seminaire = {
      ...formValue,
      id: '1', // Firestore générera automatiquement l'ID
    };

    this.firestore.collection('seminaires').add(seminaire)
      .then((docRef) => {
        console.log('Séminaire ajouté avec ID : ', docRef.id);

        this.seminaires.push(seminaire); // Mettre à jour la liste locale
        this.seminairesSubject.next(this.seminaires);
      })
      .catch((error) => {
        console.error('Erreur lors de l\'ajout du séminaire : ', error);
      });
  }

  async SupSeminaire(): Promise<void> {
    if (this.seminaires.length === 0) {
      alert('La liste des séminaires est vide.');
      return;
    }
  
    const domainesUniquesAvecNumeros = this.seminaires.reduce((acc: string[], seminaire) => {
      if (!acc.includes(seminaire.domaine)) {
        acc.push(seminaire.domaine);
      }
      return acc;
    }, []);
  
    let choixDomaine: number | null = null;
    while (choixDomaine === null) {
      const choix = prompt(
        `Choisissez le domaine du séminaire que vous souhaitez supprimer:\n` +
        domainesUniquesAvecNumeros.map((domaine, index) => `${index + 1}. ${domaine}`).join('\n')
      );
      if (choix === null) return;
      const choixIndex = parseInt(choix) - 1;
      if (!isNaN(choixIndex) && choixIndex >= 0 && choixIndex < domainesUniquesAvecNumeros.length) {
        choixDomaine = choixIndex;
      } else {
        alert('Choix invalide. Veuillez sélectionner un numéro de domaine valide.');
      }
    }
  
    const domaineChoisi = domainesUniquesAvecNumeros[choixDomaine];
    if (!domaineChoisi) {
      alert('Domaine choisi invalide.');
      return;
    }
  
    const seminairesDansDomaine = this.seminaires.filter(seminaire => seminaire.domaine === domaineChoisi);
  
    if (seminairesDansDomaine.length === 0) {
      alert(`Aucun séminaire trouvé dans le domaine "${domaineChoisi}".`);
      return;
    }
  
    let choixSeminaire: number | null = null;
    while (choixSeminaire === null) {
      const choix = prompt(
        `Choisissez le séminaire que vous souhaitez supprimer dans le domaine "${domaineChoisi}":\n` +
        seminairesDansDomaine.map((seminaire, index) => `${index + 1}. ${seminaire.titre}`).join('\n') +
        '\n\nEntrez le numéro du séminaire:'
      );
      if (choix === null) return;
      const choixIndex = parseInt(choix) - 1;
      if (!isNaN(choixIndex) && choixIndex >= 0 && choixIndex < seminairesDansDomaine.length) {
        choixSeminaire = choixIndex;
        break;
      } else {
        alert('Choix invalide. Veuillez sélectionner un numéro de séminaire valide.');
      }
    }
  
    const seminaireASupprimer = seminairesDansDomaine[choixSeminaire];
  
    // Récupérer l'ID du séminaire à partir de la base de données Firebase
    const seminaireRef = await this.firestore.collection('seminaires').ref
      .where('titre', '==', seminaireASupprimer.titre)
      .limit(1)
      .get();
  
    if (seminaireRef.empty) {
      console.error("Le séminaire n'a pas été trouvé dans la base de données.");
      return;
    }
  
    const seminaireDoc = seminaireRef.docs[0];
    const seminaireId = seminaireDoc.id;
  
    console.log('Identifiant du séminaire à supprimer :', seminaireId); // Vérifier la valeur de l'identifiant
  
    // Supprimer le séminaire de la base de données Firebase
    this.firestore.collection('seminaires').doc(seminaireId).delete()
      .then(() => {
        // Supprimer le séminaire de la liste locale
        const updatedSeminaires = this.seminaires.filter(s => s.id !== seminaireId);
        this.seminaires = updatedSeminaires;
        this.seminairesSubject.next(updatedSeminaires);
        alert(`Le séminaire "${seminaireASupprimer.titre}" a été supprimé avec succès.`);
      })
      .catch(error => {
        console.error("Erreur lors de la suppression du séminaire :", error);
        alert("Une erreur s'est produite lors de la suppression du séminaire. Veuillez réessayer.");
      });
  }
  
}

//utilisation de firebase pour les bases de données ( api de google) qui est très pratique car pas besoin de javascript pour creer le back end et donc la gestion de route pour le back end