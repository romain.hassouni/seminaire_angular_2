import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { User } from '../User/user';// Assurez-vous d'importer correctement le modèle User
import { AngularFireAuth } from '@angular/fire/compat/auth';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private firestore: AngularFirestore, private afAuth: AngularFireAuth) {}

  addUser(formValue: { name: string, prenom: string, email: string, password: string }): void {
    const user: User = {
      ...formValue,
      id: 0, // Laissez Firestore générer automatiquement l'ID
    };
    
    this.firestore.collection<User>('utilisateurs').add(user) // Ajout de l'utilisateur dans la collection 'utilisateurs' de Firestore
      .then((docRef) => {
        console.log('Utilisateur ajouté avec ID : ', docRef.id);
      })
      .catch((error) => {
        console.error('Erreur lors de l\'ajout de l\'utilisateur : ', error);
      });
  }
  async login(email: string, password: string): Promise<boolean> {
    try {
      // Récupérer les informations utilisateur correspondant à l'email
      const userDoc = await this.firestore.collection('utilisateurs', ref => ref.where('email', '==', email)).get().toPromise();
      
      if (userDoc && !userDoc.empty) {
        // Utilisateur trouvé, vérifier le mot de passe
        const userData = userDoc.docs[0].data();
        if (userData && typeof userData === 'object' && 'password' in userData) {
          if (userData.password === password) {
            // Mot de passe correspondant, authentification réussie
            return true;
          } else {
            // Mot de passe incorrect
            return false;
          }
        } else {
          // Données utilisateur invalides
          console.error('Données utilisateur invalides');
          return false;
        }
      } else {
        // Aucun utilisateur trouvé avec cet email
        return false;
      }
    } catch (error) {
      console.error('Erreur lors de la connexion :', error);
      return false;
    }
  }
}