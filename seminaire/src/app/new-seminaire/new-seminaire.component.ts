import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { ReactiveFormsModule,Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'; 
import { Seminaire } from '../models/site_models';
import { CommonModule } from '@angular/common';
import { SeminaireService } from '../services/seminaire.services';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-new-seminaire',
  standalone: true,
  imports: [ReactiveFormsModule,CommonModule,FormsModule],
  templateUrl: './new-seminaire.component.html',
  styleUrl: './new-seminaire.component.scss'
})
export class NewSeminaireComponent implements OnInit {
  
  seminaireForm!: FormGroup;
  seminairePreview$!: Observable<Seminaire>;

  constructor(private formBuilder: FormBuilder,private seminaireSerice:SeminaireService,private router:Router) {}
  
  ngOnInit(): void {
    this.seminaireForm = this.formBuilder.group({
      titre: [null, Validators.required],
      date: [null, Validators.required],
      intervenant: [null, Validators.required],
      lieu: [null, Validators.required],
      resume: [null,Validators.required],
      domaine: [null,Validators.required]//Validators.required permet la fonctionnalité du boutton submit seulement si tous les attribus ont été fournis par l utilisateur
    }, {
      updateOn: 'blur'  /*permet l ajout du champ lorsque celui-ci à été ajouté*/ 
    });

    this.seminairePreview$ = this.seminaireForm.valueChanges.pipe(
      map(formValue => {
        return {
          ...formValue,
          id:0
        }
      })
    );
  }

  onSubmitForm(): void {
    const formData = this.seminaireForm.value;
  
    // Reformater la valeur de la date
    formData.date = new Date(formData.date);
  
    this.seminaireSerice.addSeminaire(formData);
    this.router.navigateByUrl('/seminaires');
    alert(`Le séminaire "${formData.titre}" a été créé avec succès!`);
  }
}