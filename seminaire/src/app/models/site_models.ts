
import { Timestamp } from 'firebase/firestore';
export class Seminaire{


public id!:string;
public titre!:string;
        
public date!:Timestamp;
        
public intervenant!:string;
        
public lieu!:string;
        
public resume!:string;
public domaine!:string;

}

// model d un seminaire