import { ApplicationConfig } from '@angular/core';
import { provideRouter } from '@angular/router';
import { routes } from './app.routes';
import { FIREBASE_OPTIONS } from '@angular/fire/compat';
import { environment } from '../environments/environment';

export const appConfig: ApplicationConfig = {
  providers: [{provide: FIREBASE_OPTIONS, useValue: environment.firebaseConfig},provideRouter(routes),provideRouter(routes)],
  

};

export class AppComponent{

}
// configuration de firebase