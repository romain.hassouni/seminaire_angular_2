import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder } from '@angular/forms';
import { FormGroup, Validators } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-contact',
  standalone: true, // Définit si ce composant est autonome ou pas
  imports: [ReactiveFormsModule, CommonModule], // Import des modules nécessaires
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  contactForm!: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    // Initialisation du formulaire avec les champs et les validateurs
    this.contactForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      message: ['', Validators.required]
    });
  }

  onSubmitForm(): void {
    if (this.contactForm.valid) {
      // Si le formulaire est valide, soumettre les données
      console.log('Formulaire valide, soumission en cours...');
      console.log('Nom:', this.contactForm.value.name);
      console.log('Email:', this.contactForm.value.email);
      console.log('Message:', this.contactForm.value.message);

      alert('Votre formulaire a été soumis avec succès, cliquer sur f12 pour regarder la console');
    } else {
      // Si le formulaire est invalide, afficher un message d'erreur
      console.log('Formulaire invalide, veuillez vérifier les champs.');
    }
  }
}
