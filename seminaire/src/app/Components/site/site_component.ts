import { Component, OnInit, Input } from '@angular/core';
import { Seminaire } from '../../models/site_models';
import { CommonModule } from '@angular/common';
import { SeminaireService } from '../../services/seminaire.services';

import { AngularFirestore } from '@angular/fire/compat/firestore'; // Importez AngularFirestore depuis @angular/fire/compat/firestore
import { Timestamp } from 'firebase/firestore';
@Component({
  selector: 'app-site',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './site_component.html',
  styleUrl: './site_component.scss'
})
export class SiteComponent {
  @Input() seminaire!: Seminaire;

  constructor(private firestore: AngularFirestore) {} // Injectez AngularFirestore dans le constructeur

  formatDate(timestamp: any): string {
    if (timestamp instanceof Timestamp) {
        const date = timestamp.toDate();
        // Formater la date selon vos préférences
        const options: Intl.DateTimeFormatOptions = { year: 'numeric', month: 'short', day: '2-digit' };
        return date.toLocaleDateString('fr-FR', options); // Modifier 'fr-FR' selon votre locale
    } else {
        // Gérer le cas où timestamp n'est pas une instance de Timestamp
        return "Date non valide";
    }
  }

  // utilisation pour formater la date car dans la base de données firebase la date est de type timestamp et non de type date
}
