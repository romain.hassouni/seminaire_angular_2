import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Seminaire } from '../../models/site_models';
import { CommonModule } from '@angular/common';
import { SiteComponent } from '../site/site_component';
import { HeaderComponent } from '../header/header.component';
import { SeminaireService } from '../../services/seminaire.services';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms'; 
import { Observable } from 'rxjs';
import { Timestamp } from 'firebase/firestore';

@Component({
  selector: 'app-seminaire-list',
  standalone: true,
  imports: [CommonModule,SiteComponent,HeaderComponent,FormsModule],
  templateUrl: './seminaire-list.component.html',
  styleUrl: './seminaire-list.component.scss'
})
export class SeminaireListComponent implements OnInit {
  seminaires: Seminaire[] = []; // Initialisation de la variable
  seminairesFiltres: Seminaire[] = [];
  selectedDomaine: string = 'tous';
  selectedTri: string = 'recent';

  constructor(private seminaireService:SeminaireService,private router:Router) {}

  ngOnInit(): void {
    this.seminaireService.getAllSeminaire().subscribe({
      next: (seminaires: Seminaire[]) => {
        this.seminaires = seminaires; // Assigner les données récupérées
        console.log('Données récupérées :', this.seminaires); // Vérification des données récupérées
        this.applyFiltersByTous();
        this.applyFiltersByAucun();
      },
      error: (error: any) => {
        console.error("Une erreur s'est produite lors de la récupération des séminaires:", error);
      }
    });
  }
  
  getSeminaires(): void {
    this.seminaireService.getAllSeminaire().subscribe(seminaires => {
        this.seminaires = seminaires;
    });
}
applyFiltersByMath(): void {
  // Filtrer les données déjà récupérées au lieu de récupérer à nouveau
  this.seminairesFiltres = this.seminaires.filter(seminaire => {
    return seminaire.domaine === 'Math';
  });
}

applyFiltersByPhysique(): void {
  // Filtrer les données déjà récupérées au lieu de récupérer à nouveau
  this.seminairesFiltres = this.seminaires.filter(seminaire => {
    return seminaire.domaine === 'Physique';
  });
}

applyFiltersByInformatique(): void {
  // Filtrer les données déjà récupérées au lieu de récupérer à nouveau
  this.seminairesFiltres = this.seminaires.filter(seminaire => {
    return seminaire.domaine === 'Informatique';
  });
}

applyFiltersByPhilosophie(): void {
  // Filtrer les données déjà récupérées au lieu de récupérer à nouveau
  this.seminairesFiltres = this.seminaires.filter(seminaire => {
    return seminaire.domaine === 'Philosophie';
  });
}
applyFiltersByTous(): void {
  const domaineSelect = document.querySelector('.monselect2') as HTMLSelectElement;
  const selectedDomaine = domaineSelect.value;

  switch (selectedDomaine) {
    case 'tous':
      this.seminairesFiltres = this.seminaires.slice(); // Copie les données de tous les séminaires
      break;
    case 'math':
      this.applyFiltersByMath();
      break;
    case 'physique':
      this.applyFiltersByPhysique();
      break;
    case 'informatique':
      this.applyFiltersByInformatique();
      break;
    case 'philosophie':
      this.applyFiltersByPhilosophie();
      break;
    default:
      this.seminairesFiltres = this.seminaires.slice(); // Par défaut, copie toutes les données
      break;
  }
}

applyFiltersByRecent(): void {
  this.seminairesFiltres = this.seminairesFiltres.sort((a, b) => {
    // Vérifiez si les dates sont définies et sont des instances de Timestamp
    if (a.date instanceof Timestamp && b.date instanceof Timestamp) {
      return b.date.seconds - a.date.seconds; // Comparaison des timestamps en secondes
    } else {
      return 0; // Si l'une des dates est indéfinie ou n'est pas un Timestamp, ne pas modifier l'ordre
    }
  });
}

applyFiltersByAncien(): void {
  this.seminairesFiltres = this.seminairesFiltres.sort((a, b) => {
    // Vérifiez si les dates sont définies et sont des instances de Timestamp
    if (a.date instanceof Timestamp && b.date instanceof Timestamp) {
      return a.date.seconds - b.date.seconds; // Comparaison des timestamps en secondes
    } else {
      return 0; // Si l'une des dates est indéfinie ou n'est pas un Timestamp, ne pas modifier l'ordre
    }
  });
}
  
applyFiltersByAucun(): void {
  const triSelect = document.querySelector('.monselect') as HTMLSelectElement;
  const selectedTri = triSelect.value;

  switch (selectedTri) {
    case 'aucun':
      this.seminairesFiltres = this.seminaires.slice(); // Copie les données de tous les séminaires
      break;
    case 'recent':
      this.applyFiltersByRecent();
      break;
    case 'ancien':
      this.applyFiltersByAncien();
      break;
    default:
      this.seminairesFiltres = this.seminaires.slice(); // Par défaut, copie toutes les données
      break;
  }
}

applyFilters(): void {
  this.seminairesFiltres = this.seminaires.filter(seminaire => {
    return this.selectedDomaine === 'tous' || seminaire.domaine.toLowerCase() === this.selectedDomaine;
  });

  switch (this.selectedTri) {
    case 'recent':
      this.applyFiltersByRecent(); // Passer les séminaires filtrés actuels
      break;
    case 'ancien':
      this.applyFiltersByAncien(); // Passer les séminaires filtrés actuels
      break;
    default:
      // Ne rien faire si aucun tri n'est sélectionné
      break;
  }
}

  AddSeminaire(): void{
    this.router.navigateByUrl('/create');
  }
  
  SupSeminaire(): void {
    this.router.navigateByUrl('/seminaires')
    this.seminaireService.SupSeminaire();
  }
}
