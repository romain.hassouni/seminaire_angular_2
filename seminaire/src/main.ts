import { bootstrapApplication } from '@angular/platform-browser';
import { AppModule } from './app/app.module';
import { AppComponent } from './app/app.component';
import { appConfig } from './app/app.config';


type EnvironmentProviders = any; 
bootstrapApplication(AppComponent, appConfig as EnvironmentProviders);
